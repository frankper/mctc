FROM debian:bookworm-slim
#RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list

# update packages 
RUN  apt update -y \
&& apt install bash curl git wget sudo locales apt-utils -qy \
&&  apt upgrade -y

# Set and generate the locale
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -y install tzdata
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
RUN locale-gen

# Create a user with passwordless sudo
ARG USERNAME=user
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN adduser $USERNAME sudo

# Clone bootstrap repo
WORKDIR /home/$USERNAME
USER $USERNAME
RUN git clone https://gitlab.com/frankper/bootstrap-files.git utils

# install apt packages list and cleanup 
RUN ["/bin/bash", "-c", "./utils/scripts/bootstrap-system-debian-slim-container.sh"]

# copy files into the container 
RUN ["/bin/bash", "-c", "./utils/scripts/install_omb.sh"]
RUN ["/bin/bash", "-c", "./utils/scripts/bootstrap-dotfiles.sh"]
RUN ["/bin/bash", "-c", "./utils/scripts/install_yq.sh"]
RUN ["/bin/bash", "-c", "sudo rm -rf utils"]

CMD echo "starting container" && tail -f /dev/null 
