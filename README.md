Table of Contents
=================

* [Table of Contents](#table-of-contents)
* [<strong>LATEST VERSION v0.1.3</strong>](#latest-version-v010)
* [[WIP] My Cluster Test Container](#wip-my-cluster-test-container)
* [How to run this on your machine](#how-to-run-this-on-your-machine)
* [Notes](#notes)
* [Issues](#issues)
* [Tools installed](#tools-installed)
   * [network utils](#network-utils)
   * [shell utils](#shell-utils)
   * [dev utils](#dev-utils)
   * [kubernetes](#kubernetes)
   * [cloud tools](#cloud-tools)
   * [Custom dotfiles for :](#custom-dotfiles-for-)
* [Pipeline Configuration](#pipeline-configuration)
   * [Gitlab CI](#gitlab-ci)
   * [Github Actions](#github-actions)
* [Todo](#todo)
* [Repo Tree](#repo-tree)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->

# **LATEST VERSION v0.1.3**
# [WIP] My Cluster Test Container 
Used for testing new clusters based on Debian 12
Feel free to fork and modify as you please
# How to run this on your machine
By running this 
```shell
docker run -ti registry.gitlab.com/frankper/mctc
```
# Notes
* User created for the container has passwordless sudo
# Issues 
* N/A

# Tools installed 
Bootstrap scripts are [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/bootstrap-system-debian-slim-container.sh?ref_type=heads) and [apt package list is here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/system_packages_to_install_debian_slim_container.txt?ref_type=heads)
## network utils 
* dnsutils arp-scan speedtest-cli sipcalc traceroute nmap
## shell utils
* sudo wget curl vim
## dev utils
* git
## kubernetes
* jq yq kubectl
## cloud tools 
* aws-cli
## Custom dotfiles for :
*  ohMybash

# Pipeline Configuration

## Gitlab CI 
Repo contains a gitlab pipeline [file](https://gitlab.com/frankper/mctc/-/blob/main/.gitlab-ci.yml) to build and publish with every push to main
* an image with "commit sha" tag 
* an image with "latest" tag
* an image with "git tag" tag ,if present in git

## Github Actions
WIP

# Todo 
* Add tests
* Add SSH,GPG,AWS,KUBECONFIG functionality
* Correct typos in README.md as I am sure there are many :) 
* Add github actions configuration

# Repo Tree
```
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
└── README.md
```
